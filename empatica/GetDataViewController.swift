/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* this file deals with empatica connected disconnection and data collection */
import UIKit
import CoreData
import UserNotifications

class GetDataViewController: UIViewController,EmpaticaDeviceDelegate, EmpaticaDelegate {

    @IBOutlet weak var statusLabel: UILabel!
    
    private var devices: [EmpaticaDeviceManager] = []
    var startTime: Date = Date()
    var endTime: Date = Date()
    var areStarted : [Bool] = []
    var prepareTag : Timer!

    var isWearing = true
    
    var batteryLevel :Float = -1.0
    let windowSize = 60;
    var sum : Float = 0.0;
    var savePast : [[Float]] = [[],[],[],[],[]] // gsr, ibi, temp, acc
    var saveForEvent : [[(Float,Double)]] = [[],[],[]] // gsr, ibi, temp

    var totalCount : [Int] = [0,0,0,0,0] // gsr, ibi, temp, acc
    var sumAverage : [Float] = [0,0,0,0,0] // gsr, ibi, temp, acc
    var beginTime : Double = NSDate().timeIntervalSince1970
    var isConnected: Bool = false
    var  saver: dataSaver?
    var AccArray: [[Int]]=[]
    var keepMovement: Double?
    var keepAcc : [Double]?

    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            print("view getdata DidAppear")
             self.discover()
        }
    }
    

    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        print("getdata");

        areStarted.append(false) // IBI
        areStarted.append(false) // EDA
        areStarted.append(false) // TEMP
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let tabview = segue.destination as? TabViewController {
            tabview.saver = saver
        }
    }
    
    private var allDisconnected : Bool {
        print("allDisconnected")
        return self.devices.reduce(true) { (value, device) -> Bool in
            
            value && device.deviceStatus == kDeviceStatusDisconnected
        }
    }

    
    private func discover() {
         print("discover")
        EmpaticaAPI.discoverDevices(with: self)
    }
    
    private func disconnect(device: EmpaticaDeviceManager) {
          print("disconnect")
        if device.deviceStatus == kDeviceStatusConnected {
            
            device.disconnect()
        }
        else if device.deviceStatus == kDeviceStatusConnecting {
            
            device.cancelConnection()
        }
    }
    
    private func connect(device: EmpaticaDeviceManager) {
             print("connect")
        if(!device.isFaulty && device.allowed){
            EmpaticaAPI.cancelDiscovery()
            
            device.connect(with: self)
        }
        else
        {
            restartDiscovery()
        }
    }
    
    
    
    private func deviceStatusDisplay(status : DeviceStatus) -> String {
        print("deviceStatusDisplay")

        switch status {
            
        case kDeviceStatusDisconnected:
            return "Disconnected"
        case kDeviceStatusConnecting:
            return "Connecting..."
        case kDeviceStatusConnected:
            return "Connected"
        case kDeviceStatusFailedToConnect:
            return "Failed to connect"
        case kDeviceStatusDisconnecting:
            return "Disconnecting..."
        default:
            return "Unknown"
        }
    }
    
    private func restartDiscovery() {
        
        print("restartDiscovery")
        
        guard EmpaticaAPI.status() == kBLEStatusReady else { return }
        
        if self.allDisconnected {
            
            print("restartDiscovery • allDisconnected")
            
            self.discover()
        }
    }



    
    func didDiscoverDevices(_ devices: [Any]!) {
        
        print("didDiscoverDevices")
        
        if self.allDisconnected {
            
            print("didDiscoverDevices • allDisconnected")
            
            DispatchQueue.main.async {
                
                if( devices.count > 0 )
                {       self.connect(device:devices[0] as!  EmpaticaDeviceManager)
                }
                else{
                    EmpaticaAPI.discoverDevices(with: self)
                }
            }
        }
        
    }
    
 
    func didUpdate(_ status: BLEStatus) {
        
        switch status {
        case kBLEStatusReady:
            print("[didUpdate] status \(status.rawValue) • kBLEStatusReady")
            statusLabel.text = "ready to pair";
            break
        case kBLEStatusScanning:
            print("[didUpdate] status \(status.rawValue) • kBLEStatusScanning")
            statusLabel.text = "scanning";

            break
        case kBLEStatusNotAvailable:
            print("[didUpdate] status \(status.rawValue) • kBLEStatusNotAvailable")
            break
        default:
            print("[didUpdate] status \(status.rawValue)")
        }
  
    }


    func didReceiveBatteryLevel(_ level : Float , withTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {
        print("didReceiveBatteryLevel" + String(level))

        batteryLevel = level

        if(level < 0.4 && level != batteryLevel && (Int(level*100)%10 == 0))
        {
            
            let content = UNMutableNotificationContent()
            content.title = "Battery level low"
            content.body = "Battery level is " + String(level*100.0) + "%, consider charging the device"
            content.sound = UNNotificationSound.default()
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
        
    }
    
    
    @objc func prepareForTag()
    {
        print("prepareForTag")
        saver!.saveInEvents(startDate: Date().addingTimeInterval(-2.5 * 60.0),endDate: Date().addingTimeInterval(2.5 * 60.0),isPressed: true) // save 2,5 min ago to 2,5 min from now
        let notificationNme = NSNotification.Name(rawValue: "reloadTable") // and reload the event table
        NotificationCenter.default.post(name: notificationNme, object: nil)
        
    }
    // the user pressed the empatica button, add an envent to the mandatory list with a time range of 2.5 min before and after the pressing time
    // do not add it right away, wait 2 second to see if the empatica turns off. If so it was a turn off press and not a tag press. Do not add in the mandatory list in this case ( see disconnection part )
    func didReceiveTag(atTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {
        print("didReceiveTag")
        // user pressed the button
           DispatchQueue.main.async() {
            self.prepareTag = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.prepareForTag), userInfo: nil, repeats: false)
        }
        //print("\(device.serialNumber!) TAG received { \(timestamp) }")
    }
    
    func didReceiveGSR(_ gsr: Float, withTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {

        while( !saver!.isAvailable[0])
        {
            print("waiting for rawdata availability")
        }
        saver!.isAvailable[0] = false
        saver!.rawDatas[0].append( dataType(data: gsr, timeStamp: timestamp)  )
        saver!.isAvailable[0] = true
        
        //print("\(device.serialNumber!) GSR { \(abs(smoothedVal)) , \(timestamp) }")

    }
    func  didReceiveIBI(_ ibi: Float, withTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {
        saver!.saveIBI.append([timestamp,Double(ibi)])
     
        while( !saver!.isAvailable[1])
        {
            print("waiting for rawdata availability")
        }
        saver!.isAvailable[1] = false
        saver!.rawDatas[1].append( dataType(data: ibi, timeStamp: timestamp)  )
        saver!.isAvailable[1] = true
      
        //print("\(device.serialNumber!) IBI { \(abs(ibi))  , \(timestamp) }")
    }
    func didReceiveTemperature(_ temp: Float, withTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {
        isConnected = true;

       if let index = saveForEvent[2].index(where: { $0.0 < temp }) {
            saveForEvent[2].insert((temp,timestamp), at: index)
        }
        else{
            saveForEvent[2].append((temp,timestamp))
        }
        
    
        while( !saver!.isAvailable[2])
        {
            print("waiting for rawdata availability")
        }
        saver!.isAvailable[2] = false
        saver!.rawDatas[2].append( dataType(data: temp, timeStamp: timestamp)  )
        saver!.isAvailable[2] = true
        
        
      //  print("\(device.serialNumber!) TEMP { \(smoothedVal)  , \(timestamp) }")
        // you may want to do some detection of weither or not the user is wearing the device. I didn't use this in the end. be careful of seasons...
        
       // if(temp > 27)
       // {
            isWearing = true
           // eventDetected( index: 2, val: smoothedVal, threshold: 31.0, sup: false)

     //   }else{
    //        isWearing = false
     //   }
    }
    func didReceiveAccelerationX(_ x: Int8, y: Int8, z: Int8, withTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {
        isConnected = true;
        // save the acceleration raw data and calculate the movement index to use for calculating additional heart rate for prompting
        AccArray.append( [Int(x),Int(y),Int(z)])

        if( AccArray.count == 32)
        {

          saver!.MovementArray.append( getMovement(ACCdata: AccArray))

            if(saver!.MovementArray.count > 32*60)
            {
                saver!.MovementArray.removeFirst()
            }
            AccArray=[]
        }
       
        
        while( !saver!.isAvailable[3])
        {
            print("waiting for rawdata availability")
        }
        saver!.isAvailable[3] = false
        saver!.rawDatas[3].append( dataType(x: Int16(x), y: Int16(y),z: Int16(z), timeStamp: timestamp)  )
        saver!.isAvailable[3] = true
        
        
      //  print("\(device.serialNumber!) ACC > {x: \(x), y: \(y), z: \(z) , \(timestamp) }")
    }
    func didReceiveBVP(_ bvp: Float, withTimestamp timestamp: Double, fromDevice device: EmpaticaDeviceManager!) {
        isConnected = true;

        let new = dataType(data: bvp, timeStamp: timestamp)

        while( !saver!.isAvailable[4])
        {
            print("waiting for rawdata availability")
        }
        saver!.isAvailable[4] = false
        saver!.rawDatas[4].append( new )
        saver!.isAvailable[4] = true
         //print("\(device.serialNumber!) BVP { \(bvp)  , \(timestamp) }")
    }
    

    
    func didUpdate( _ status: DeviceStatus, forDevice device: EmpaticaDeviceManager!) {
        print("didUpdate func")
        
        switch status {
            
        case kDeviceStatusDisconnected:
            
            print("disconnected ok")
            // if we had a detected press but it was actually a turning off, stop the adding to the mandatory list
            if(prepareTag != nil)
            {
                prepareTag.invalidate()
            }
            if(!saver!.isStopped())
            {
                // create a notification
                let content = UNMutableNotificationContent()
                content.title = "Empatica disconnected"
                content.body = "Please reconnect to Empatica"
                content.sound = UNNotificationSound.default()
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                
                let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
            
            // go to begining
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "startView")
            (controller as! startController).saver = saver
            self.present(controller, animated: false, completion: nil)

          
            AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            
            break
            
        case kDeviceStatusConnecting:

            print("[didUpdate] Connecting \(device.serialNumber!).")
            break
            
        case kDeviceStatusConnected:

            print("[didUpdate] Connected \(device.serialNumber!).")
            beginTime = NSDate().timeIntervalSince1970
            setTimeWear()
      
            
            statusLabel.text = "connected";
            DispatchQueue.main.async() {
                [unowned self] in
              self.performSegue(withIdentifier: "showTabView", sender: self)
            }
            break
            
        case kDeviceStatusFailedToConnect:

            print("[didUpdate] Failed to connect \(device.serialNumber!).")
            
           
            DispatchQueue.main.async {
                self.restartDiscovery()
            }
            break
            
        case kDeviceStatusDisconnecting:

            print("[didUpdate] Disconnecting \(device.serialNumber!).")
            
            break
            
        default:
            break
            
        }
    }

    // moving window average ( not used in this app)
    func averageWindow( indexTypeData : Int, newData : Float) -> Float
    {
        totalCount[indexTypeData] = totalCount[indexTypeData] + 1


           sumAverage[indexTypeData] = sumAverage[indexTypeData] + newData;

        savePast[indexTypeData].append(newData)

        if( totalCount[indexTypeData] >= windowSize)
        {
            if(totalCount[indexTypeData] != windowSize)
            {
                sumAverage[indexTypeData] = sumAverage[indexTypeData] - savePast[indexTypeData][0];
                savePast[indexTypeData].removeFirst()
            }
            return  sumAverage[indexTypeData]/Float(windowSize);
            
        }
        else{
           return  newData
        }
    }
    
    /*
     this function permits to keep track of when the empatica is being worn so we can calculate how long it was worn today and therefore how long is left in the experiment
 */
    
    func setTimeWear()
    {
        print("setTimeWear")
        do {
            guard let appDelegate  =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
                // get last moment worn
                var fetchRequest = NSFetchRequest<NSManagedObject>(entityName:"WearingTime")
                let lastTime = try managedContext.fetch(fetchRequest)
                print("lastTime",lastTime.count)
                
                if lastTime.count != 0
                {
                    let  lastentry :Double = saver!.getLastSave()
                  
                    if lastentry != -1
                    {
                        if(lastentry >  (lastTime[lastTime.count - 1].value(forKeyPath: "start") as! Double))
                        {
                         lastTime[lastTime.count - 1].setValue(lastentry, forKey: "finish")
                        }else{
                            managedContext.delete(lastTime[lastTime.count - 1])
                        }
                    }
                    else{
                        for i in 0...lastTime.count-1{
                            managedContext.delete(lastTime[i])
                        }
                    }
                }
            

            // save new begining of wearing time
            let entity = NSEntityDescription.entity(forEntityName: "WearingTime",
                                                    in: managedContext)!
            let event = NSManagedObject(entity: entity,
                                        insertInto: managedContext)
            event.setValue(NSDate().timeIntervalSince1970, forKeyPath: "start")
            
            try managedContext.save()
            
             fetchRequest =
                NSFetchRequest<NSManagedObject>(entityName: "WearingTime")
             try managedContext.fetch(fetchRequest)

              
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    /////////calculate movement from acc data
    // Each second (32 samples) the acceleration data is summarized using he following method:
    //   sum+= max3(abs(buffX[i] - prevX), abs(buffY[i] - prevY), abs(buffZ[i] - prevZ));
    //  The output is then filtered:
    //  avg=avg*0.9+(sum/32)*0.1;
    // returns the movement array
    
    

    
    // takes a new 32*3  values of ACC and calculate the result movement
    
    func getMovement(ACCdata: [[Int]]) -> Double
    {
        
        
        var sum : Double = 0.0
        var tmpACC : [Double]
        var tmpACC2 : [Double]
        for i in (0...31){
            tmpACC = [(Double(ACCdata[i][0])*2.0)/128,(Double(ACCdata[i][1])*2.0)/128,(Double(ACCdata[i][2])*2.0)/128]
            if(i==0 && keepAcc == nil)
            {
                sum = sum + max(abs(tmpACC[0]), abs(tmpACC[1]), abs(tmpACC[2]));
            }
            else if(i==0 )
            {
                sum = sum + max(abs(tmpACC[0] - keepAcc![0]), abs(tmpACC[1] - keepAcc![1]), abs(tmpACC[2] - keepAcc![2]));
            }else{
                tmpACC2 = [(Double(ACCdata[i-1][0])*2.0)/128,(Double(ACCdata[i-1][1])*2.0)/128,(Double(ACCdata[i-1][2])*2.0)/128]
             
                sum = sum + max(abs(tmpACC[0] - tmpACC2[0]), abs(tmpACC[1] - tmpACC2[1]), abs(tmpACC[2] - tmpACC2[2]));
            }
            keepAcc = tmpACC
        }
        sum = sum/32
        var res : Double
        if( keepMovement == nil)
        {
            res = sum
        }
        else{
            res = keepMovement!*0.9 + sum*0.1
        }
        keepMovement = res
        return res
        
    }

}

    
    


