/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* THIS IS AN OLD FILE, not used anymore, I have left it in case someone wants to use it. It permits to display emoticons "sad" "happy "angry" "fear" "neutral" and some example of emotions */
import UIKit
import CoreData
import DLRadioButton

class MandatoryViewController: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var exampleSentence: UILabel!
    @IBOutlet weak var valueStrength: UILabel!
    @IBOutlet weak var slider: UISlider!

    @IBOutlet weak var comment: UITextField!
    @IBOutlet weak var startTime: UIDatePicker!
    
    @IBOutlet weak var endTime: UIDatePicker!
    @IBOutlet weak var peakButton: DLRadioButton!
    @IBOutlet weak var radioButtons: DLRadioButton!
    @IBOutlet weak var peakTime: UIDatePicker!
    @IBOutlet weak var date: UILabel!
    
    var keyboardHeight : CGFloat?

    
    var sentences = [[""]];
    var peakOption = false;
    var sentencesHappy = ["It is the end of the day, You get to go home","Someone gives me a compliment","Meeting a good friend/receiving a gift","Your superior congratulate me for your hard work ", "You meet my boyfriend/girlfirend that you haven't seen in a while / Your paper has been accepted"]
    var sentencesSad = ["You realized you lost something not so valuable","The cancelation of something you were expecting","One of your friend moves away","Your paper got rejected","Someone you are close to just died/ you are crying"]
    var sentencesAngry = ["The person in front of you is being slow","Noisy colleague","Noisy person while you try to sleep","Someone criticize you","Having a loud argument with someone / You are yelling"]
    var sentencesFear = ["Sudden noise","You are watching a scary movie","Your boyfriend/girlfied tells you 'we need to talk'","PhD defense/ big presentation/ carrer or life affecting event", "A car almost ran you over/threat of your life"]
    
    var emotions = ["angry","sad","neutral","fear","happy"]
    var event: NSManagedObject?

     override func viewDidLoad() {
        super.viewDidLoad()
        
        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: 65)

        comment.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(ToAddViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ToAddViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        
        (self.view as! UIScrollView).isScrollEnabled = false
        
        let startD: Date = event!.value(forKeyPath: "fromDate") as! Date;
        let endD: Date = event!.value(forKeyPath: "toDate") as! Date;
      
        let dateFormatterGet = DateFormatter();
        dateFormatterGet.dateFormat = "dd/MM/YYYY";
        let dateString : String = dateFormatterGet.string(from: startD);

        
        date.text = dateString;
        
        startTime.date = startD;
        endTime.date = endD;

        radioButtons.otherButtons[1].isSelected = true;
         sentences = [sentencesAngry,sentencesSad,sentencesFear,sentencesHappy];
        slider.tintColor = UIColor.gray;
        slider.isEnabled = false;

        peakTime.minimumDate = startD;
        peakTime.maximumDate = endD;
        //peakTime.isUserInteractionEnabled = false;
        peakTime.isEnabled = false
        timesChange ()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //AppUtility.lockOrientation(.landscape, andRotateTo: .landscape)
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    

    @IBAction func sliderChanged ()
    {
        let roundedValue  = round(slider.value )
        slider.value = roundedValue
       // print(roundedValue)
        valueStrength.text = String(Int(roundedValue))
        reload();
    }
    @IBAction func emotionChanged ()
    {print("motion changed");
        reload();
        slider.value = 3;
        sliderChanged ();
    }
    func reload()
    {
       let emotionIndex = Int(radioButtons.selected()!.titleLabel!.text!)!;
        if(emotionIndex == 4)
        {
            exampleSentence.text = ""
            slider.isEnabled = false;
            slider.tintColor = UIColor.gray;
        }else{
            slider.isEnabled = true;
            exampleSentence.text = sentences[emotionIndex][Int(valueStrength.text!)! - 1 ]
            slider.tintColor = UIColor.blue;

        }
        
    }
    
    @IBAction func peakOptionChanged ()
    {print("peakOption");
        if(peakOption){
            peakButton.isSelected = false;
            peakTime.isEnabled = false;
        }
        else
        {
            peakTime.isEnabled = true;
        }
        peakOption = !peakOption
        timesChange ()
    }
    
    @IBAction func timesChange ()
    {
        endTime.minimumDate = startTime.date;
        
        let calendar = Calendar.current
        endTime.maximumDate = calendar.date(byAdding: .minute, value: 5, to: Date())! // max date is now + 5 min
        startTime.maximumDate = Date()

        peakTime.minimumDate = startTime.date;
        peakTime.maximumDate = endTime.date;
 
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn");
        comment.resignFirstResponder()
        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: 0)

        return true
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if keyboardHeight != nil {
            
            return
            
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardHeight = keyboardSize.height
            // so increase contentView's height by keyboard height
            (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + keyboardHeight! )
            
            
            // move if keyboard hide input field
            let distanceToBottom = (self.view as! UIScrollView).frame.size.height - (comment.frame.origin.y) - (comment.frame.size.height)
            
            let collapseSpace = keyboardHeight! - distanceToBottom
            
            if collapseSpace < 0 {
                
                // no collapse
                return
            }
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                
                // scroll to the position above keyboard 10 points
                (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: collapseSpace + 10)
            })
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardHeight = nil
    }
}
