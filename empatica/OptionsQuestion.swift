/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* deals with every question views */
import Foundation
class OptionsQuestion: UIViewController {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answer1Label: UIButton!
    @IBOutlet weak var answer2Label: UIButton!
    @IBOutlet weak var answer3Label: UIButton!
    var questionTree : Tree?
    var  answers : [String] = []
    var event: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        setup(t:questionTree!)
    }
    
    /* setup the view according the the question tree*/
    func setup(t: Tree)
    {
        // if there is no more question
        if(t.isLeaf)
        {
            // add the answer and change view
            answers.append(t.question!)
            
            if(event!.isLong){
                self.performSegue(withIdentifier: "askLabel", sender: self)
            }
            else{
                    if(answers[0]=="neutral")
                    {
                        self.performSegue(withIdentifier: "askLabel", sender: self)
                    }
                    else{
                        self.performSegue(withIdentifier: "showRate", sender: self)
                    }
            }
        }
        else{ // if there is still questions
            // if the question contains the <time> string, change it for the actual time and display the question
            if(t.question!.contains("<time>")){
                let dateFormatterGet = DateFormatter();
                dateFormatterGet.dateFormat = "HH:mm";
                questionLabel.text = t.question!.replacingOccurrences(of: "<time>", with: dateFormatterGet.string(from: event!.dateEnd))
            }
                else
                {
                    questionLabel.text = t.question!
                }
            // display the answers
            answer1Label.setTitle(t.answers![0], for: .normal)
            answer2Label.setTitle(t.answers![1], for: .normal)
            // if there is more than 2 answer , display the 3 answer label
            if(t.answers!.count > 2){
                answer3Label.setTitle(t.answers![2], for: .normal)
                answer3Label.isEnabled = true
            }
            else{
                answer3Label.setTitle("", for: .normal)
                answer3Label.isEnabled = false
            }
        }
    }
    
    
    // go to the next question
    @IBAction func next(sender: UIButton)
    {
        answers.append(questionTree!.answers![sender.tag])

        let tmp  = questionTree!.ress![sender.tag]
            if( tmp.count>1 && answers[0]=="negative")
            {
                setup(t:tmp[1])
                questionTree=tmp[1]
            }
            else{
                setup(t:tmp[0])
                questionTree=tmp[0]
            }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? Rating{
            destination.answers = answers
            destination.event = event

        }
        if let destination = segue.destination as? askLabel{
            destination.answers = answers
            destination.event = event
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear optionsQuestions")
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
}


