/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/* this deals with the strength view */

import Foundation
class Rating: UIViewController,UITextFieldDelegate {
 
    var answers : [String] = []
    
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var sliderRes: UILabel!
    @IBOutlet weak var comment: UITextField!
    var   event : Event?
    var keyboardHeight : CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: 65)
        
        comment.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        (self.view as! UIScrollView).isScrollEnabled = false
        
        if(answers[answers.count-1] == "no emotion" || answers[answers.count-1] == "sport")
        {
           question.isHidden = true
           slider.isHidden = true
            sliderRes.isHidden = true
        }
    }
    @IBAction func next()
    {
        event!.strength =  Int(round(slider.value ))
        
        if(event!.isLong){
            print("enter islong")
            if( event!.strength! > 1){
                self.performSegue(withIdentifier: "showQuestions", sender: self)
            }
            else{ // if the duration is long but the strength is < 1 it is probably a mood, save it as mood
                answers.append("Mood")
               // saveEMotion()
                self.performSegue(withIdentifier: "askLabel", sender: self)

            }
        }else{
          //  saveEMotion()
            self.performSegue(withIdentifier: "askLabel", sender: self)

        }
    }
    // listen to any slider change. Only allow for rounded values ( 1, 2 , 3 )
    @IBAction func sliderChanged ()
    {
        let roundedValue  = round(slider.value )
        slider.value = roundedValue
        sliderRes.text = String(Int(roundedValue))
    }
    
    // deals with hiding the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn");
        comment.resignFirstResponder()
        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: 0)
        
        return true
    }
    
    // deals with dispalying the keyboard and moving the view up
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardHeight = keyboardSize.height
            // so increase contentView's height by keyboard height
            (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + keyboardHeight! )
            
            // move if keyboard hide input field
            
            let distanceToBottom = (self.view as! UIScrollView).frame.size.height - (comment.frame.origin.y) - (comment.frame.size.height)
            
            let collapseSpace = keyboardHeight! - distanceToBottom
            
            if collapseSpace < 0 {
                
                // no collapse
                return
            }
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                
                // scroll to the position above keyboard 10 points
                (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: collapseSpace + 10)
            })
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardHeight = nil
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        event!.comment = comment.text
    
        if let destination = segue.destination as? fourOptions{
            destination.event = event
            destination.answers = answers
        }else if let destination = segue.destination as? askLabel{
            destination.event = event
            destination.answers = answers
            
        }
       
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear rating")
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
}


