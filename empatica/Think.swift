/*
This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
< paper currently in publication, please wait for the details >
    
    MIT License
Copyright (c) [2019] [Fanny Larradet]
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
/* this file is displayed when the user select an event in the mandatory list*/
import CoreData
import Foundation
class Think: UIViewController {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    var event: NSManagedObject?
    var   ev : Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        let startD: Date = event!.value(forKeyPath: "fromDate") as! Date;
        let endD: Date = event!.value(forKeyPath: "toDate") as! Date;
        
        let dateFormatterGet = DateFormatter();
        dateFormatterGet.dateFormat = "dd/MM/YYYY";
        date.text = dateFormatterGet.string(from: startD);

        dateFormatterGet.dateFormat = "HH:mm";
        startTime.text = dateFormatterGet.string(from: startD);
        endTime.text = dateFormatterGet.string(from: endD);

        ev = Event(dateStart: startD, dateEnd: endD,coreEvent :event! )
        ev!.isPressed = event!.value(forKeyPath: "isPressed") as! Bool;
    }
    
    @IBAction func next ()
    {
        self.performSegue(withIdentifier: "showValence", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? Valence{
            destination.event = ev
            print("think")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear think")
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }

}
