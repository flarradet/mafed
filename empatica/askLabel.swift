/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* at the end of the OCC tree, ask for a user picked label*/
import Foundation
class askLabel: UIViewController {
    @IBOutlet weak var happyBtn: UIButton!
    @IBOutlet weak var sadBtn: UIButton!
    @IBOutlet weak var angryBtn: UIButton!
    @IBOutlet weak var noEmotion: UIButton!
    var answers : [String] = []

    

    var   event : Event?
 
 
    override func viewDidLoad() {
        
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

    }
 
    
    @IBAction  func saveEMotion(_ sender: UIButton)
    {
        event!.emotion = answers.joined(separator: "/")
        
        if let buttonTitle = sender.title(for: .normal) {
            
            event!.selfLabel = buttonTitle

            // save the emotion
            saver!.saveEventInEmotions(event: event!)
            // remove the event from the list if it is a mandatory event
            if(event!.coreEvent != nil ){
                saver!.removeEvent(event: event!.coreEvent!)
            }
            // reload chart
            let notificationNme = NSNotification.Name(rawValue: "reloadChart")
            NotificationCenter.default.post(name: notificationNme, object: nil)
            navigationController?.popToRootViewController(animated: true)
            saver!.newData=true
        }
    
    }
  
  // randomise the list
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        var emotions=["happy","sad","angry","no emotion"]
        
        var diceRoll = Int(arc4random_uniform(4))
        happyBtn.setTitle( emotions[diceRoll], for: .normal)
        emotions.remove(at: diceRoll)
        
        diceRoll = Int(arc4random_uniform(3) )
        sadBtn.setTitle( emotions[diceRoll], for: .normal)
        emotions.remove(at: diceRoll)
        
        diceRoll = Int(arc4random_uniform(2) )
        angryBtn.setTitle( emotions[diceRoll], for: .normal)
        emotions.remove(at: diceRoll)

        
        noEmotion.setTitle( emotions[0], for: .normal)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
 
}


