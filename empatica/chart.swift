/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/* this file deals with the graph tab*/

import UIKit
import Charts
import CoreData

class chart: UIViewController, ChartViewDelegate {

    @IBOutlet weak var barChartView: BarChartView!
    
    var months: [String]!
    var emotions: [NSManagedObject] = []
    var referenceDate : Date?
    var saver: dataSaver?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
         referenceDate = formatter.date(from: "2015/12/31 00:00")
        let gdvc = self.presentingViewController as! GetDataViewController
         saver = gdvc.saver
        emotions = saver!.getSavedEmotions()
   
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1

        // display 5 days starting from the set begining time of experiement ( see coreDataManager)
        let timeStart = formatter.date(from: saver!.DateStartExperiment+" 00:00")
        let timeEnd = timeStart!.addingTimeInterval(432000)
        
    
        xAxis.axisMinimum = getMinutes( fromD:referenceDate!, toD: timeStart!);
        xAxis.axisMaximum = getMinutes( fromD:referenceDate!, toD: timeEnd);
        xAxis.valueFormatter = DayAxisValueFormatter(chart: barChartView)
        barChartView.setVisibleXRangeMaximum(1440)
        barChartView.setVisibleXRangeMinimum(1440)

        xAxis.labelCount = 12
        xAxis.granularity = 120
        xAxis.granularityEnabled = true

        var i = 0
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MM/yyyy"
        // add day lines
        while xAxis.axisMinimum + Double(1440*i) < xAxis.axisMaximum
        {
            let tmpDate = NSCalendar.current.date(byAdding: .day, value: i, to: timeStart!)
            let ll = ChartLimitLine(limit:  xAxis.axisMinimum + Double(1440*i), label: formatter2.string(from: tmpDate!))
            barChartView.xAxis.addLimitLine(ll)
            i=i+1
        }
        

        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 1
        leftAxisFormatter.maximumFractionDigits = 1

        
        let leftAxis = barChartView.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0
        leftAxis.granularity = 1
        leftAxis.axisMaximum = 3
        let numberFormater = NumberFormatter()
        numberFormater.generatesDecimalNumbers = false
        leftAxis.valueFormatter = numberFormater as? IAxisValueFormatter
        
        let rightAxis = barChartView.rightAxis
        rightAxis.enabled = false

        
        let l = barChartView.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4

        
         XYMarkerView(color: UIColor(white: 180/250, alpha: 1),
                                  font: .systemFont(ofSize: 12),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: barChartView.xAxis.valueFormatter!)
        barChartView.scaleYEnabled = false

        barChartView.autoScaleMinMaxEnabled = false

        setChartData();
        
        barChartView.chartDescription?.enabled = false
        
        
        let notificationNme = NSNotification.Name("reloadChart")
        NotificationCenter.default.addObserver(self, selector: #selector(chart.refreshData), name: notificationNme, object: nil)
        
  
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // set the orientation in landscape
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)

    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
    }
    
    // display all the event saved using their positive/ negative value
    func setChartData() {

        let chartData = BarChartData()
        var emotionMap = [String : [BarChartDataEntry] ]()
        
        let emotionsLabel = ["positive","negative"]

        let colors = [NSUIColor.orange,NSUIColor.blue]

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MM/yyyy HH:mm"
        for index in 0 ... emotionsLabel.count-1
        {
            let dataEntries: [BarChartDataEntry] = []
            emotionMap[emotionsLabel[index]] = dataEntries
        }
            for  entry:NSManagedObject in emotions
            {


                let fromDate :Date = entry.value(forKeyPath: "fromDate") as! Date
                let toDate :Date = entry.value(forKeyPath: "toDate") as! Date
                let strength :Double = entry.value(forKeyPath: "strength") as! Double
                let emotionLabel: String = String(entry.value(forKeyPath: "emotion") as! String).components(separatedBy: "/")[0]

                if(emotionLabel != "neutral")
                {
                    let minutes = getMinutes(fromD:referenceDate!,  toD: fromDate)
                    for  i in 0  ... Int(getMinutes(fromD:fromDate,  toD: toDate))
                    {  
                        emotionMap[emotionLabel]!.append(BarChartDataEntry(x: minutes + Double(i), y: strength))
                    }
                }
            }


        for index in 0 ... emotionsLabel.count-1
        {
            let chartDataSet = BarChartDataSet(values: emotionMap[emotionsLabel[index]] , label: emotionsLabel[index])
            chartDataSet.drawValuesEnabled = false
            chartDataSet.setColor(colors[index], alpha: CGFloat(1.0))
            chartData.addDataSet(chartDataSet)
        }

        barChartView.data = chartData

    }

    
    func getMinutes( fromD:Date , toD: Date) -> Double
    {
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.minute], from: fromD, to: toD)
        return Double(components.minute!)
    }
    

    @objc func refreshData()
    {
        print("refreshData")

        emotions =   saver!.getSavedEmotions()
        setChartData();
        barChartView.notifyDataSetChanged(); // let the chart know its data changed
        barChartView.data?.notifyDataChanged()

    }
    
  
}
struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
}
