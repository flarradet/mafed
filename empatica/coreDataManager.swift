/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* this is the most important file of the app.It deals with all core data manipulation */

import Foundation
import CoreData
import UserNotifications
import AVFoundation

class dataSaver {
    var audioPlayer: AVAudioPlayer?
    // set the beginging date of the experiment at the begining of each subject
    var DateStartExperiment : String = "2019/04/01" // (YY/MM/DD)
    
    // this time is the starting time of the experiment every day. The app will start reminding the user to wear empatica every day starting from this time. set it up in accordance with your project and/or the user
    var HourStartDay : String   = " 08:00" //(HH:MM)

    var managedContext : NSManagedObjectContext?
    var events: [NSManagedObject] = []
    var newData:Bool = false

    var rawDatas :[[dataType]] = [[],[],[],[],[]] // gsr, ibi, temp, acc , bvp
    var isAvailable : [Bool] = [true,true,true,true,true]
    var namesEntry: [String] = ["GSREntry", "IBIEntry","TempEntry","AccEntry", "BVPEntry"]
    
    

    var MovementArray : [Double] = []
    var saveIBI : [[Double]] = []
    var lastPrompt : Double = 0
    var Q : Int = 30
    var QCountShort = 0
    var QCountLong = 0
    
    var QAverage = 0
    var lastEvent : Double = 0
    var lastCheck: Double?

    
    init()
    {
        // because ios do not permit to continu the scripts to run in the background, I had to play a silent file in order for this script to continue to run even when the phone is locked.
        do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            if let fileURL = Bundle.main.path(forResource: "1-hour-of-silence", ofType: "mp3") {

                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileURL))
                audioPlayer?.numberOfLoops = -1
                audioPlayer?.play()

            } else {
                print("No file with specified name exists")
            }
        } catch let error {
            print("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
        
        guard let appDelegate  =
        UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        managedContext = appDelegate.persistentContainer.viewContext

        // remind the user to report its mandatory events every 15 min (if any)
         Timer.scheduledTimer(timeInterval: 15*60, target: self, selector: #selector(dataSaver.notifyMandatory), userInfo: nil, repeats: true)
        
        // prompt the user when a new mandatory event is set
        Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(shouldPrompt), userInfo: nil, repeats: true)
        // save the signals raw data to the phone core data every minute
        Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(saveRawData), userInfo: nil, repeats: true)


    }
    // we detected an emotion , if not in conflict with something already registered, prompt
    func saveEvent(startDateEvent : Date, endDateEvent: Date )
    {
        print("start saveEvent")
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SavedEmotion")
        
        DispatchQueue.global(qos: .default).async {
            
            let threadContext : NSManagedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
            threadContext.parent = self.managedContext;
            
            var doPrompt = true

                    print("savedata available")
                // check if there is already a report at this time
                // V is for voluntary M mandatory, check all possibilities (I used to do difference thing in each case. Now no. I left it in case it can be useful for someone)
            
                    // case  : V   M   V   M
                    fetchRequest.predicate = NSPredicate(format: "(fromDate < %@) AND (toDate > %@ AND toDate < %@) AND emotion!=%@", startDateEvent as CVarArg, startDateEvent as CVarArg, endDateEvent as CVarArg, "neutral")
                    
                    do {
                        
                        self.events = (try threadContext.fetch(fetchRequest))
                        
                        if(self.events.count > 0)
                        {
                            print("VMVM")
                            //dont prompt , there is already a Voluntary event at this time
                            doPrompt=false

                        }
                    } catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                    
                    // case  :    M  V  M  V
                    fetchRequest.predicate = NSPredicate(format: "(fromDate > %@ AND fromDate < %@) AND (toDate > %@) AND emotion!=%@", startDateEvent as CVarArg, endDateEvent as CVarArg, endDateEvent as CVarArg, "neutral")
                    do {
                        self.events = (try threadContext.fetch(fetchRequest))
                        
                        if(self.events.count > 0)
                        {  print("MVMV")
                            doPrompt=false

                           //dont prompt , there is already a Voluntary event at this time
                        }
                    } catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                    
                    
                    
                    
                    // case  :    M  V V  M
                    fetchRequest.predicate = NSPredicate(format: "(fromDate > %@ AND fromDate < %@) AND (toDate > %@ AND toDate < %@) AND emotion!=%@", startDateEvent as CVarArg, endDateEvent as CVarArg,startDateEvent as CVarArg,  endDateEvent as CVarArg, "neutral")
                    do {
                        self.events = (try threadContext.fetch(fetchRequest))
                        
                        if(self.events.count > 0)
                        {      print("MVVM")
                            doPrompt=false

                            //dont prompt , there is already a Voluntary event at this time
                        }
                        
                    } catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                    
                    
                    
                    // case  :   V M M V
                    fetchRequest.predicate = NSPredicate(format: "fromDate < %@ AND toDate > %@ AND emotion!=%@", startDateEvent as CVarArg,  endDateEvent as CVarArg, "neutral")
                    do {
                        self.events = (try threadContext.fetch(fetchRequest))
                        
                      if(self.events.count > 0){
                            print("VMMV")
                        doPrompt=false
                        //dont prompt , there is already a Voluntary event at this time

                        }
                    } catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                    
    
                    // create a notification if an event has been added
                    if(doPrompt)
                    {
                        self.saveInEvents(startDate: startDateEvent, endDate: endDateEvent,isPressed: false)

                        let notificationNme = NSNotification.Name(rawValue: "reloadTable") // and reload the event table
                        NotificationCenter.default.post(name: notificationNme, object: nil)
     
                        let content = UNMutableNotificationContent()
                        content.title = "Please register your emotion"
                        content.body = "What are you feeling now?"
                        content.sound = UNNotificationSound.default()
                        
                        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                        
                        let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
                        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                    }
                
            
        }
        print("end savedata")
    }
    
    
    /* notify that there is a mandatory event to fill up*/
    @objc func notifyMandatory()
    {
        print("start notifyMandatory")
        
        
        let threadContext : NSManagedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        threadContext.parent = self.managedContext;
        
    
            if(!isStopped() )
            {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "EmotionalEvent")
                
                do{

                        let count = try threadContext.count(for: fetchRequest)
                    // if there is any event in the list send a reminder
                        if( count > 0)
                        {
                            // create a notification
                            let content = UNMutableNotificationContent()
                            content.title = "Please register your emotion"
                            content.body = "You have unAnswered mandatory emotions !! Please answer them ASAP"
                            content.sound = UNNotificationSound.default()
                            
                            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                            
                            let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
                            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                        }
                    } catch let error as NSError {
                        print("Could not fetch. \(error), \(error.userInfo)")
                    }
                }
            
        print("end notifyMandatory")
    }
    
    /* we detected an event, save it to the list */
    func saveInEvents(startDate: Date, endDate: Date,isPressed:Bool)  {
        
        print("start saveInevent")
            let entity =
                NSEntityDescription.entity(forEntityName: "EmotionalEvent",
                                           in: self.managedContext!)!
            
            let event = NSManagedObject(entity: entity,
                                        insertInto: managedContext)
            
            
            event.setValue(startDate, forKeyPath: "fromDate")
            event.setValue(endDate, forKeyPath: "toDate")
            event.setValue(isPressed, forKeyPath: "isPressed")
            
            do {
                
                try self.managedContext?.save()
                print("end saveInevent")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }

    }
    func resetAllRecords(in entity : String)
    {
        
        let context = ( UIApplication.shared.delegate as! AppDelegate ).persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do
        {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch
        {
            print ("There was an error")
        }
        
    }
    
    /* save raw data to the core data*/
    @objc func saveRawData ( )
    {
        DispatchQueue.global(qos: .default).async {
            
            print("start saverawdata")
            let threadContext : NSManagedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
            threadContext.parent = self.managedContext;
            /* to ensure there is no access of the arrays at the save time */
            while( !self.isAvailable[0] || !self.isAvailable[1] || !self.isAvailable[2] || !self.isAvailable[3] || !self.isAvailable[4] )
            {
                print("waiting for rawdata availability in saverawdata")
            }
            for i in 0 ... 4
            {
           self.isAvailable[i] = false
            }
                let tmpData : [[dataType]] = self.rawDatas
            for i in 0 ... 4
            {
                self.isAvailable[i] = true
            }
            var i = 0
            
                for arrayType: [dataType] in tmpData
                {

                    var j = 0
                    for data:dataType in arrayType
                    {
                        let entity =
                            NSEntityDescription.entity(forEntityName: self.namesEntry[i],
                                                       in: threadContext)!
                        
                        let event = NSManagedObject(entity: entity,
                                                    insertInto: threadContext)
                        if(self.namesEntry[i]=="AccEntry")
                        {
                            event.setValue(data.x, forKeyPath: "x")
                            event.setValue(data.y, forKeyPath: "y")
                            event.setValue(data.z, forKeyPath: "z")
                            event.setValue(data.timeStamp, forKeyPath: "timeStamp")
                        }
                        else
                        {
                            event.setValue(data.data, forKeyPath: "data")
                            event.setValue(data.timeStamp, forKeyPath: "timeStamp")
                        }
                        j = j+1
                    }
                    i = i+1
                }
                
                
                
                do {
              
 
                    try threadContext.save()
                    print("saverawdata after save context")

                    for i in 0 ... tmpData.count - 1
                    {
                        if(tmpData[i].count > 0)
                        {
                            let tmp : Double = (tmpData[i][tmpData[i].count - 1]).timeStamp
                            while( !self.isAvailable[i])
                            {
                                print("waiting for rawdata availability in saverawdata")
                            }
                            self.isAvailable[i] = false
                            self.rawDatas[i] = self.rawDatas[i].filter() { $0.timeStamp > tmp }
                            self.isAvailable[i] = true

                        }
                    }
                    try self.managedContext!.save()

                    print(" saved successfuly " )
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }

            print("end saverawdata")
        }
    }
    
    /* save the event report*/
    func saveEventInEmotions(event: Event) {

        print("saveEventInEmotions")

        saveInEmotions( emotion:event.emotion!, startDate: event.dateStart, endDate: event.dateEnd, strength: event.strength! , optionalComment: event.comment! , isPressed: event.isPressed , selfLabel : event.selfLabel! , isMandatory: event.mandatory)
        
    }
    
    /* save the event report*/
    func saveInEmotions( emotion:String, startDate: Date, endDate: Date, strength: Int, optionalComment: String,isPressed: Bool,selfLabel:String,isMandatory:Bool) {
            print("start saveInEmotions")

        let threadContext : NSManagedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        threadContext.parent = self.managedContext;
        
        
                let entity =
                    NSEntityDescription.entity(forEntityName: "SavedEmotion",
                                               in: threadContext)!
            
                let event = NSManagedObject(entity: entity,
                                            insertInto: threadContext)
                event.setValue(emotion, forKeyPath: "emotion")
        

        
                event.setValue(startDate, forKeyPath: "fromDate")
                event.setValue(endDate, forKeyPath: "toDate")
                event.setValue(strength, forKeyPath: "strength")
                event.setValue(optionalComment, forKeyPath: "comment")

                event.setValue(Date(), forKeyPath: "timeSaved")
                event.setValue(isMandatory, forKeyPath: "isMandatory")
                event.setValue(isPressed, forKeyPath: "isPressed")
                event.setValue(selfLabel, forKeyPath: "selfLabel")

                do {

                    try threadContext.save()
                    try self.managedContext!.save()

                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            
            print("end saveInEmotions")

    }
    
    /* check if there is already an emotion report saved at those times */
    func checkValidity(fromTime: Date, untilTime: Date )  -> Bool {
        print("start checkValidity")

                var events: [NSManagedObject] = []
        
                let fetchRequest =
                    NSFetchRequest<NSManagedObject>(entityName: "SavedEmotion")
        
                fetchRequest.predicate = NSPredicate(format: "(fromDate < %@) AND (toDate > %@) AND emotion!=%@", untilTime as CVarArg, fromTime as CVarArg, "neutral")

                do {
                events = try self.managedContext!.fetch(fetchRequest)
                    return events.count == 0
                } catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                    return false
                }
    }
    
    /* check if there is already a mandatory event saved  at those times */
    func verifyAccess() -> Bool{
        print("start verifyAccess")

                var events: [NSManagedObject] = []

                let fetchRequest =
                    NSFetchRequest<NSManagedObject>(entityName: "EmotionalEvent")
                do {
                    events = try self.managedContext!.fetch(fetchRequest)
                    print("end verifyAccess")
                    return events.count==0
                } catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                    return false
                }
    }
    
    /* get the mandatory list */
    func getEmotionalEvents() ->  [NSManagedObject] {
        print("start getEmotionalEvents")

                let fetchRequest =
                    NSFetchRequest<NSManagedObject>(entityName: "EmotionalEvent")

                do {
                    events = try self.managedContext!.fetch(fetchRequest)
                    print("end getEmotionalEvents")
                    return events
                } catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                    return[]
                }
    }
    /* get the saved emotion reports list */
    func getSavedEmotions() -> [NSManagedObject]
    {
        print("start getSavedEmotions")

                let fetchRequest =
                    NSFetchRequest<NSManagedObject>(entityName: "SavedEmotion")
                do {
                   let  emotions = try self.managedContext!.fetch(fetchRequest)
                    return emotions
                } catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                    return []
                }
    }
    
    /* remove an event from the mandatory list */
    func removeEvent(event : NSManagedObject)
    {
            print("start removeEvent");

        self.managedContext!.delete(event);
        
                do{
                    try self.managedContext!.save();
                }catch{}
                let notificationNme = NSNotification.Name(rawValue: "reloadTable")
                NotificationCenter.default.post(name: notificationNme, object: nil)
        print("end removeEvent");
    }
    
    /* save a temporary stop */
    func saveStop(startDate: Date, endDate: Date, comment:String) {
        print("start saveStop")
        
            let entity =
                NSEntityDescription.entity(forEntityName: "StopTimes",
                                           in: self.managedContext!)!
            
            let event = NSManagedObject(entity: entity,
                                        insertInto: managedContext)
            
            
            event.setValue(startDate, forKeyPath: "timeStart")
            event.setValue(endDate, forKeyPath: "timeStop")
            event.setValue(comment, forKeyPath: "comment")

            do {
                
                try self.managedContext?.save()
                print("end saveStop")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
    
    }
    /* calculate how long the person has worn the band today */
    func getHowLongWornToday()  -> Double
    {
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        // Get today's beginning & end
        var dateFrom = calendar.startOfDay(for: Date())
        print(dateFrom)
       return getHowLongWornSinceDate(dateFrom: dateFrom)
    }
    
    /* calculate how long the person has worn the band since a specific date */
    func getHowLongWornSinceDate(dateFrom: Date) -> Double
    {
        
        print("start getHowLongWornSinceDate")
        let threadContext : NSManagedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        threadContext.parent = self.managedContext;
        do {
            
            
            
            // get the wearing times of today
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WearingTime")
            
            fetchRequest.predicate = NSPredicate(format: "(start > %@) AND (finish != 0.0) ", NSNumber(value: Double(dateFrom.timeIntervalSince1970) ))
            
            var  worns = try threadContext.fetch(fetchRequest)
            
            var count : Double = 0
            var wn=0
            print("nb wear with end ")
            print(worns.count)
            while wn < worns.count{
                print("wearingtimeEntry")
                count = count + Double((worns[wn].value(forKeyPath: "finish") as! Double) - (worns[wn].value(forKeyPath: "start") as! Double))
                wn += 1
                print( count)
            }
            
            print("middle" + String(count))
            fetchRequest.predicate = NSPredicate(format: "(start > %@) AND  (finish == 0.0) ", NSNumber(value: Double(dateFrom.timeIntervalSince1970) ))
              worns = try threadContext.fetch(fetchRequest)
            
            for w in worns{
                print(w)
            }
            
            
            print("nb wear no end ")
            print( worns.count)
            if(worns.count>0)
            {
                count = count + Double(NSNumber(value: Double(Date().timeIntervalSince1970))) - (worns[worns.count-1].value(forKeyPath: "start") as! Double)
  
            }
            else{
                count = Double(NSNumber(value: Double(Date().timeIntervalSince1970))) - Double(dateFrom.timeIntervalSince1970)
            }
            print("fin" + String(count))

            return count
        } catch _ as NSError {
            print("error getHowLongWornSinceDate")
            return 0
        }
    }
    
    /* see if the experiemnt is temporarly stopped*/
    func isStopped() -> Bool{
        
        
        print("start isStopped")
        let threadContext : NSManagedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        threadContext.parent = self.managedContext;

        do {
            
           let count = getHowLongWornToday()

            if(count > 12*60*60){ // if the person already wore the bracelet for 12H
                print("experiment done")
                return true // then consider the experiment stopped for today
            }
        
        
        // if it is too early to start the experiment according to the set hour start time
        let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
        let today =  formatter.string(from: Date())
            formatter.dateFormat = "yyyy/MM/dd HH:mm"
            

        if( Date().compare(formatter.date(from: today + HourStartDay )!) == ComparisonResult.orderedAscending )
        {
            print("experiment not started")
            return true
        }

        //  if the experiment is temporary stopped

        let fetchRequestStop = NSFetchRequest<NSManagedObject>(entityName: "StopTimes")
        fetchRequestStop.predicate = NSPredicate(format: "( %@> timeStart ) AND ( %@ < timeStop )", Date() as CVarArg, Date() as CVarArg)
        
           let  countI = try threadContext.count(for: fetchRequestStop)
 
        let fetchRequestTmp = NSFetchRequest<NSManagedObject>(entityName: "StopTimes")

            try threadContext.fetch(fetchRequestTmp)

            
            if(countI > 0 )
            {
                return true
            }
        } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
        }
        print("experiment not stopped, notify")

        // else it is not stopped
        return false;

    }
    /* event detection using additional heart rate method */
    @objc func shouldPrompt()
    {
        print( "shouldPrompt")
        //if we have enough data to do the calculation
        if ( (MovementArray.count >= 4*60))
        {
            var i=0
            var previousIBI: Double = 0
            var  nowIBI : Double = 0
            var countPreviousIBI :Int = 0
            var countNowIBI : Int = 0
            while i < saveIBI.count
            {
                print(saveIBI[i])
                // if its more than 4 min before now, remove it
                if( saveIBI[i][0] < NSDate().timeIntervalSince1970 -  4*60)
                {
                    saveIBI.remove(at: i)
                }// if its less than 4 min before now, but more than 1 min before now , add the previousIBI
                else  if(( saveIBI[i][0] >  NSDate().timeIntervalSince1970 - 4*60  ) &&  ( saveIBI[i][0] <  NSDate().timeIntervalSince1970 - 60  ))
                {
                    previousIBI = previousIBI + saveIBI[i][1]
                    countPreviousIBI = countPreviousIBI + 1
                    i=i+1
                }
                else{
                    nowIBI = nowIBI + saveIBI[i][1]
                    countNowIBI = countNowIBI + 1
                    i=i+1
                }
            }
            previousIBI = 60.0 / (previousIBI/Double(countPreviousIBI))
            nowIBI = 60.0 / (nowIBI/Double(countNowIBI))
            var totalPA : Double = 0
            for i in 0...MovementArray.count-1
            {
                totalPA = totalPA + MovementArray[i]
            }
            let PA = totalPA/Double(MovementArray.count)
            

            if(nowIBI > previousIBI + (PA*8000 + 500)/Double(Q))
            {
                
                QCountLong = 0
                // if its less than 1m later we consider it as the same event do not count
                if(!(lastEvent + 60 > NSDate().timeIntervalSince1970)){
                    // otherwise increment the event counter
                    QCountShort = QCountShort+1
                    QCountLong = QCountLong+1
                }
                
                lastEvent = NSDate().timeIntervalSince1970
                
                
                
                // if its been an hour since last prompt
                if (lastPrompt + 3600 < NSDate().timeIntervalSince1970  )
                {
                    //prompt
                    saveEvent(startDateEvent : Date().addingTimeInterval(-2*60+30), endDateEvent:Date().addingTimeInterval(2*60+30) )
                    
                    lastPrompt = NSDate().timeIntervalSince1970
                    // if we found an event more than 3 time an hour,  diminuish the Q
                    if( QCountShort > 4)
                    {
                        Q =  max(0, Q-1)

                    }
                    QCountShort = 0
                }

            }

            print( " wearing time since last event= " + String(getHowLongWornSinceDate(dateFrom: NSDate(timeIntervalSince1970: lastEvent) as Date)))
            
            print("lastEvent = " + String(lastEvent))
            
            
            let   maxHourNoPrompt : Double =  3*3600

            if(lastCheck == nil )
            {

                if(lastEvent != 0 )
                {
                    if(getHowLongWornSinceDate(dateFrom: NSDate(timeIntervalSince1970: lastEvent) as Date) > maxHourNoPrompt )
                    {
                         //time since lastEvent longer
                        Q = Q + 1
                        lastCheck = Date().timeIntervalSince1970
                    }

                }
                else if(getHowLongWornToday() > maxHourNoPrompt )
                {
                    //time worn today longer
                        Q = Q + 1
                    lastCheck = Date().timeIntervalSince1970
                }
            }
            else if(getHowLongWornSinceDate(dateFrom: NSDate(timeIntervalSince1970: lastCheck!) as Date) > maxHourNoPrompt)
            {
                //time worn since last check longer

                if(lastEvent != 0 )
                {
                    //lastEvent not 0

                    if(getHowLongWornSinceDate(dateFrom: NSDate(timeIntervalSince1970: lastEvent) as Date) > maxHourNoPrompt )
                    {
                        //time worn since lastEvent longer

                        Q = Q + 1
                        lastCheck = Date().timeIntervalSince1970
                    }
                    
                }
                else
                {
                   //last event is 0

                        Q = Q + 1
                    lastCheck = Date().timeIntervalSince1970
                }
                
            }
            
         
        }
    }
    
    /* get the last date of the saved gsr entry  */
    func getLastSave() -> Double
    {
       
        do {
            guard let appDelegate  =
                UIApplication.shared.delegate as? AppDelegate else {
                    return -1
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            
      
               let fetchRequest =
                    NSFetchRequest<NSManagedObject>(entityName: "GSREntry")
                
                let sort = NSSortDescriptor(key: "timeStamp", ascending: true)
                fetchRequest.sortDescriptors = [sort]
                let  lastentry = try managedContext.fetch(fetchRequest)

            
                print("lastentry.count",lastentry.count)
                if lastentry.count != 0
                {
                    return (lastentry[lastentry.count - 1].value(forKeyPath: "timeStamp") as! Double)
                }
                else{
                  print("no last entry")
                    return -1
                }
                
            }
        catch{
            return -1
        }
    }

}

