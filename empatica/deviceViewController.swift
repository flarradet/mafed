/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* this deals with the info tab */

import UIKit

class deviceViewController: UIViewController {

    @IBOutlet weak var lastSave: UILabel!
    @IBOutlet weak var timeLeft: UILabel!
    @IBOutlet weak var batteryLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        let notificationNme = NSNotification.Name("updateBattery")
        NotificationCenter.default.addObserver(self, selector: #selector(deviceViewController.updateBattery), name: notificationNme, object: nil)
        
        }
    
    /* if the view appears, get the  battery level and how long the user have worn the device today to calculate how long is left */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        let wornTime = saver!.getHowLongWornToday()
        print(wornTime)
        let countTimeLeft = Int( 12*60*60 - wornTime)
        let h = Int(countTimeLeft / 3600)
        let m = Int((countTimeLeft - h*3600) / 60 )
            
        timeLeft.text = String(h) + "h " + String(m) + "m"
        
        let gdvc = self.presentingViewController as! GetDataViewController
        if(gdvc.batteryLevel != -1)
        {
            batteryLabel.text = String(Int(gdvc.batteryLevel*100)) + "%"
        }
    }
    
    @objc func updateBattery ( notification: NSNotification)
    {
        print("update battery")
        
        if let level = notification.userInfo?["level"] as? Float {
            print("update battery" +  String(Int(level*100)) + "%" )
            DispatchQueue.main.async {
                self.batteryLabel.text = String(Int(level*100)) + "%"
            }
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
