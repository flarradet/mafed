/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* this deal with the first question view "person", "object" "event" ... */


import Foundation
class fourOptions: UIViewController {
    

    @IBOutlet weak var Question: UILabel!
    var mainTree : Tree = Tree()

    var answers : [String] = []
    var   event : Event?

    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        mainTree = Tree().getMainTree()
        Question.text = "I feel "+answers[0]+"ly toward:" // negatively, positively according to the answer
    }
    
    @IBAction func next(sender: UIButton)
    {
        answers.append(mainTree.answers![sender.tag])

        
        
        if( mainTree.ress![sender.tag].count>1 && answers[0]=="negative")
        {
            self.performSegue(withIdentifier: "showQuestion", sender: mainTree.ress![sender.tag][1])
        }
        else{
            self.performSegue(withIdentifier: "showQuestion", sender: mainTree.ress![sender.tag][0])
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? OptionsQuestion{
        guard let tree = sender as? Tree  else { return }
          
            destination.questionTree = tree
            destination.answers = answers
            destination.event = event

        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear four options")
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
}


