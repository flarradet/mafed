/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* this contains the OCC question tree , the event model and data model */
import Foundation
import CoreData

/* occ tree*/
class Tree {
    var question: String?
    var answers: [String]?

    var ress: [[Tree]]?

    var isLeaf : Bool = false
    init() {
    }
    init(question: String,answers: [String],ress:[[Tree]] ) {
        self.question = question
        self.answers = answers
        self.ress=ress
    }
 
    init(question: String) {
        self.question = question
        isLeaf=true
    }
   func getMainTree() -> Tree{
        
        let t1 : Tree = Tree(question: "The consequences are ",answers: ["Desirable for others", "Undesirable for others"],ress: [[Tree(question: "Happy-for"),Tree(question: "resentment")],  [Tree(question: "gloating") ,Tree(question: "pity")] ])
        let t2 : Tree = Tree(question: "The expectation was ",answers: ["Confirmed", "Disconfirmed"],ress:  [[Tree(question: "satisfaction") , Tree(question: "fear-confirmed")], [Tree(question: "relief") , Tree(question: "disapointment")]] )
    let t3 : Tree = Tree(question: "",answers: ["You did something that lead to the event", "Someone did something that lead to the event","None of the above"],ress: [[Tree(question: "gratification") , Tree(question: "remorse")] ,  [Tree(question: "gratitude") , Tree(question: "anger")] , [Tree(question: "joy") , Tree(question: "distress")]])
    
    let t4 : Tree = Tree(question: "Did you have expectations for this event? ",answers: ["Yes", "No"],ress: [[t2], [t3]] )
        let t5 : Tree = Tree(question: "At the moment of this emotion (<time>) , the event was ",answers:[ "Past", "Futur"],ress: [[t4], [Tree(question: "Hope") , Tree(question: "fear")]])
        let treeEvent = Tree(question: "This event mainly had/will have consequences",answers: ["for me", "for someone else"],ress: [[t5], [t1]])

        let TreeYourselfPos =  Tree(question: "Is there/ will it be positive consequences?",answers: ["Yes", "No"],ress:[ [Tree(question: "Gratification")], [Tree(question: "pride")]])
        
        let TreeYourselfNeg =  Tree(question: "Is there/ will it be negative consequences? ",answers:[ "Yes", "No"],ress: [[Tree(question: "remorse")],  [Tree(question: "shame")] ])
        
    let TreeOtherPos  =  Tree(question: "is there / will it be positive consequences for you" ,answers:[ "Yes", "No"],ress:[ [Tree(question: "gratitude")],  [Tree(question: "admiration")]] )
        
        let TreeOtherNeg  =  Tree(question:"is there / will it be negative consequences for you",answers:[ "Yes", "No"],ress:[ [Tree(question: "anger")],  [Tree(question: "reproach")] ])


        
        let mainTree = Tree(question: "I feel negatively/positively toward:" , answers:[ "event", "other agent", "self agent", "object","none"], ress: [[treeEvent],[TreeOtherPos,TreeOtherNeg],[TreeYourselfPos,TreeYourselfNeg],[Tree(question: "Love"),Tree(question: "Hate")],[Tree(question: "Mood")]])
    
          return mainTree

    }
 
}

// event model
class Event {
    var dateStart: Date
    var dateEnd: Date
    var emotion: String?
    var comment: String?
    var strength: Int?
    var coreEvent: NSManagedObject?
    var isLong: Bool
    var mandatory: Bool
    var isPressed: Bool
    var selfLabel:String?

    init(dateStart: Date, dateEnd: Date, coreEvent: NSManagedObject) {
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        self.coreEvent = coreEvent
        self.isLong=false
        self.mandatory=true
        self.isPressed = false
        self.strength=0
        self.comment=""
    }
    
    init(dateStart: Date, dateEnd: Date) {
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        self.isLong=false
        self.mandatory=false
        self.isPressed = false
        self.strength=0
        self.comment=""
    }
}

// data model 
class dataType {
    
    var data : Float = 0.0
    var x : Int16 = 0
    var y : Int16 = 0
    var z : Int16 = 0
    var timeStamp : Double
    
    init(data: Float, timeStamp : Double)
    {
        self.data = data;
        self.timeStamp = timeStamp
    }
    init(x: Int16, y: Int16,z: Int16,timeStamp : Double)
    {
        self.x = x;
        self.y = y;
        self.z = z;
        self.timeStamp = timeStamp
    }
    
}
