import pandas as pd
from numpy.random import random, seed
import numpy as np
import matplotlib.pyplot as plt
import os
import json
#On importe les differentes couches
from keras.models import Sequential 
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense, Dropout

#On importe les pertes
from keras.losses import mean_squared_error, binary_crossentropy
from keras.layers import BatchNormalization

#On importe les optimizers
from keras.optimizers import Adam, SGD


###########################
######Read data with pandas
###########################

Xdata = []
Xtrain = pd.read_csv("data/modelEmpatica_filtered_binary.txt",header=0,delimiter=";")
Xtest = pd.read_csv("data/testEmpatica_filtered_binary.txt",header=0,delimiter=";")

col_keep = ['HR','EDA','ST','Acc1','Acc2','Acc3']

ytrain = Xtrain[['label']]
Xtrain = Xtrain[col_keep]
ytest = Xtest[['label']]
Xtest = Xtest[col_keep]

ytrain = np.array(ytrain)
Xtrain = np.array(Xtrain)
ytest = np.array(ytest)
Xtest = np.array(Xtest)
#print('Labels detected:',set(list(ytrain)),set(list(ytest)))
print("shape of training XP:",Xtrain.shape,"with",len([u for u in ytrain if u==1])/ytrain.shape[0],"positive")
print("shape of testing XP:",Xtest.shape,"with",len([u for u in ytest if u==1])/ytest.shape[0],"positive")


###########################
######Train set construction
###########################

length = 10#time series length

n = 10000#approximate number of data for train
n_size = Xtrain.shape[0]

##construction des TS
Xtrain_final = []
ytrain_final = []

#train set construction
start_1_train = []#store position of train examples with positive label
start_0_train = []#store position of train examples with negative label
for k in range(2*n):
    start = int(np.random.choice(n_size-length,1))
    if np.sum(ytrain[start:(start+length)])>0.8*length:
        ytrain_final.append(1)
        start_1_train.append(start)
        Xtrain_final.append(Xtrain[start:(start+length)])
for k in range(n):
    start = int(np.random.choice(n_size-length,1))
    if np.sum(ytrain[start:(start+length)])<-0.8*length:
        ytrain_final.append(0)
        start_0_train.append(start)
        Xtrain_final.append(Xtrain[start:(start+length)])

Xtrain_final = np.array(Xtrain_final)
ytrain_final = np.array(ytrain_final)

print("Size of training volume=",Xtrain_final.shape)
print("ratio train =",np.sum(ytrain_final)/ytrain_final.shape[0])


###########################
######Test set construction
###########################
'''
Dans la construction du test set, on propose deux methodes :
-l'une "random" qui est equivalente a la construction du train set,
et qui consiste a choisir aleatoirement des morceaux dans le fichier test
-l'une "chrono" qui consiste a decouper le fichier test de maniere chronologique
pour simuler un experience de test reelle ou chaque moment de x second est donne au CNN pour predire l'emotion.
'''

testset_construction = 'chrono'
#testset_construction ='random' 

#test set construction chrono mode
n_size_test = Xtest.shape[0]
Xtest_final = []
ytest_final = []
M = int(n_size_test/length)
if testset_construction == 'chrono':
    Xtest_final = [Xtest[k*length:(k+1)*length] for k in range(M)]
    for k in range(M):
        if np.sum(ytest[k*length:(k+1)*length])>0.8*length:
            ytest_final.append(1)
        elif np.sum(ytest[k*length:(k+1)*length])<-0.8*length:
            ytest_final.append(0)
        else:
            #print("undefined class")
            ytest_final.append(0.5)

#test set construction random mode
start_1_test = []#store position of train examples with positive label
start_0_test = []#store position of train examples with negative label

if testset_construction == 'random':
    for k in range(8*n):
        start = int(np.random.choice(n_size_test-length,1))
        if np.sum(ytest[start:(start+length)])>0.8*length:
            start_1_test.append(start)
            ytest_final.append(1)
            Xtest_final.append(Xtest[start:(start+length)])
    for k in range(n):
        start = int(np.random.choice(n_size_test-length,1))
        if np.sum(ytest[start:(start+length)])<(-0.8*length):
            start_0_test.append(start)
            ytest_final.append(0)
            Xtest_final.append(Xtest[start:(start+length)])

Xtest_final = np.array(Xtest_final)
ytest_final = np.array(ytest_final)

print("Size of test volume=",Xtest_final.shape)
print("ratio test =",np.sum(ytest_final)/ytest_final.shape[0])

###########################
######CNN Training
###########################
'''
Ici on entraine un reseau de neurones convolutionnels avec Keras. Le reseau propose a 7 couches, 4 couches de convolutions
puis 3 couches Denses. On l'entraene sur 10 epochs et Keras affiche les performances sur le train set et sur le test set
apres chaque epoch.
'''

n,T,p = Xtrain_final.shape
Xtrain_ = Xtrain_final.reshape((n,T,p,1)) #On adapte les donnees pour preciser qu'il n'y a qu'un seul channel
n_test,T,p = Xtest_final.shape
Xtest_ = Xtest_final.reshape((n_test,T,p,1))

nbreOutputFilter1 = 256
nbreOutputFilter2 = 64

sizeWindow = (3,1)
stridesWindow = (1,1)
maxPoolingSize = (2,1)



model = Sequential()


model.add(Conv2D(nbreOutputFilter1, sizeWindow, strides=stridesWindow, padding='valid', data_format="channels_last", activation=None, use_bias=True, bias_initializer='zeros'))
model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
model.add(Activation('relu'))
model.add(Conv2D(nbreOutputFilter1, sizeWindow, strides=stridesWindow, padding='valid', data_format="channels_last", activation=None, use_bias=True, bias_initializer='zeros'))
model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))

model.add(Activation('relu'))

model.add(Conv2D(nbreOutputFilter2, sizeWindow, strides=stridesWindow, padding='valid', data_format="channels_last", activation=None, use_bias=True, bias_initializer='zeros'))
model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
model.add(Activation('relu'))


model.add(Conv2D(nbreOutputFilter2, sizeWindow, strides=stridesWindow, padding='valid', data_format="channels_last", activation=None, use_bias=True, bias_initializer='zeros'))
model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
model.add(Activation('relu'))

model.add(Flatten())
model.add(Dense(units=32, activation='relu'))
#model.add(Dense(units=90, activation='relu'))
model.add(Dense(units=8, activation='relu'))

model.add(Dense(units=1, activation='sigmoid'))

optimizer = Adam(lr=1e-5, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
#optimizer = SGD(lr=1, momentum=0.0, decay=0.0, nesterov=False)
#model.compile(optimizer, metrics=['acc'], loss=mean_squared_error)
model.compile(optimizer, metrics=['acc'], loss=binary_crossentropy)

print("Training your Emotion - CNN started...")
history = model.fit(Xtrain_, ytrain_final, validation_data=(Xtest_,ytest_final), epochs=10, batch_size=256)


###############
##Analyzing predictions
###############

predictions = model.predict(Xtest_)
#print(predictions)
plt.plot(predictions,c='r')
plt.plot(ytest_final,c='g')
plt.axhline(y=0.5, color='b', linestyle='-')

#plt.ylim((0,1))
#plt.xlim((1800,2500))
#plt.savefig("fig_1800to2500.png")
plt.show()


print(history.history.keys())
#  "Accuracy"
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()
# "Loss"
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()
