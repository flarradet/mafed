/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
import UIKit
import CoreData
import UserNotifications

class startController: UIViewController {
    
    var disconnectedNotif: Timer!
    var  saver: dataSaver?

    override func viewDidLoad() {
        
        super.viewDidLoad()
     
        print("start controller" )
        // every 10 seconds send a reminder to sync
         disconnectedNotif = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(startController.sendReminder), userInfo: nil, repeats: true)

        
    }
    
    @objc func sendReminder()
    {
        print("start sendReminder" )


        if((saver) != nil)
            {
                print("saver not nil")

            if(!saver!.isStopped())
            {
                print("saver not stopped")

                let content = UNMutableNotificationContent()
                content.title = "Empatica disconnected"
                content.body = "you turned off the device or it is too far from the phone."
                content.sound = UNNotificationSound.default()
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                
                let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
        }
    }
    

    @IBAction func goToSync(sender: AnyObject) {
        disconnectedNotif.invalidate();
        self.performSegue(withIdentifier: "showSyncView", sender: self)
    
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? GetDataViewController{
            destination.saver = saver
        }
    }
    
    
}





