/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/* this permits the user to temporarly stop the notifications ( sport , shower, meeting ... )
 */
import Foundation
class stopManager: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var comment: UITextField!
    
    @IBOutlet weak var timePeaker: UIDatePicker!
    var keyboardHeight : CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        timePeaker.minimumDate = Date();
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: 65)

        
        comment.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        (self.view as! UIScrollView).isScrollEnabled = false

        
        // if already stop, cancel the stop
    }
    @IBAction func TimeChanged()
    {

        timePeaker.minimumDate = Date();
    }
    @IBAction func save()
    {
    // save it in the database and disable notifications
        saver!.saveStop(startDate: Date(), endDate: timePeaker.date, comment:comment.text!)
        self.tabBarController?.selectedIndex = 0;
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardHeight = keyboardSize.height
            // so increase contentView's height by keyboard height
            (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + keyboardHeight! )

            // move if keyboard hide input field
            let distanceToBottom = (self.view as! UIScrollView).frame.size.height - (comment.frame.origin.y) - (comment.frame.size.height)
            
            let collapseSpace = keyboardHeight! - distanceToBottom
            
            if collapseSpace < 0 {
                
                // no collapse
                return
            }
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                
                // scroll to the position above keyboard 10 points
                (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: collapseSpace + 10)
            })
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn");
        comment.resignFirstResponder()
        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        (self.view as! UIScrollView).contentOffset = CGPoint(x: 0, y: 0)
        
        return true
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        
        keyboardHeight = nil
        
    }
}
