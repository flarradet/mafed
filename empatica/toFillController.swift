/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
/* this file controls the mandatory tab*/

import UIKit
import CoreData

var events: [NSManagedObject] = []
var saver : dataSaver?
class toFillController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("view did load mandatory")

        let gdvc = self.presentingViewController as! GetDataViewController
         saver = gdvc.saver
        // get the mandatory list to display it
        events =  saver!.getEmotionalEvents()
        
        // listen to other files trying to reload this page
        let notificationNme = NSNotification.Name("reloadTable")
        NotificationCenter.default.addObserver(self, selector: #selector(toFillController.reloadTableview), name: notificationNme, object: nil)
      
    }
    
    // reload the list
    @objc func reloadTableview() {
        print("reload");
        events = saver!.getEmotionalEvents()
        self.tableView.reloadData();
    }
    
    // force the orientation to portrait
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return(events.count)
    }

    // displays the mandatory list
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath);

        let startD: Date = events[indexPath.row].value(forKeyPath: "fromDate") as! Date;
        let endD: Date = events[indexPath.row].value(forKeyPath: "toDate") as! Date;

        let dateFormatterGet = DateFormatter();
        dateFormatterGet.dateFormat = "dd/MM'   from:  'HH:mm'     to: '";
        let dateStringStart : String = dateFormatterGet.string(from: startD);
        
        dateFormatterGet.dateFormat = "HH:mm";
        let dateStringEnd : String = dateFormatterGet.string(from: endD);
        
        
        cell.textLabel?.text = dateStringStart + dateStringEnd;
        return cell
 
       
    }
    
    // when moving to the mandatory report part, send which row was clicked
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? Think{
            destination.event = events[(tableView.indexPathForSelectedRow?.row)!]
        }
    }


    
    override lazy var refreshControl: UIRefreshControl? = {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action:
            #selector(toFillController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        print("refresh");
        events = saver!.getEmotionalEvents()
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
}
