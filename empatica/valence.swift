/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
 < paper currently in publication, please wait for the details >
 
 MIT License
 Copyright (c) [2019] [Fanny Larradet]
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/* deals with the positive/ negative/ neutral view*/
import Foundation
class Valence: UIViewController {
    var   event : Event?

    @IBOutlet weak var neutralBtn: UIButton!
    @IBOutlet weak var neutralEmoticon: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

     if(!event!.mandatory)
     {neutralEmoticon.isHidden=true
        neutralBtn.isHidden=true
        }
    }
    @IBAction func next(sender: UIButton)
    {
         if(event!.isLong)
            {
                self.performSegue(withIdentifier: "showStrength", sender: sender.tag)
            }
            else{
                if(sender.tag==0 || sender.tag==1){
                    self.performSegue(withIdentifier: "showChoices", sender: sender.tag)
                }
                else{
                    self.performSegue(withIdentifier: "askSport", sender: sender.tag)
                }
            }
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let answers : [String]
           guard let tag = sender as? Int  else { return }
        if(tag==0){
            answers = ["positive"]
        }
        else{
            answers = ["negative"]
        }
        
        
        if let destination = segue.destination as? fourOptions{
         
            destination.answers = answers
            destination.event = event
        }
       else if let destination = segue.destination as? OptionsQuestion{
                    let noneTree  =  Tree(question: "Were you doing a physical or mental activity  ?",answers:[ "A physical activity", "A mental activity", "no, nothing in particular"],ress:[ [Tree(question: "sport")], [Tree(question: "mental")], [Tree(question: "no emotion")] ])
            event!.comment=""
            destination.answers = ["neutral"]
            destination.questionTree = noneTree
            destination.event = event

        }
        else if let destination = segue.destination as? Rating{
            destination.answers = answers
            destination.event = event
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear valence")
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
   
}


