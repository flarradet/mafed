/*
 This project has been send to the ACII 2019 conference . This is an open source project completly free of charge. If you want to use it as part of your research please cite the following paper:
< paper currently in publication, please wait for the details >

 MIT License
Copyright (c) [2019] [Fanny Larradet]
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 
*/
/* deals with the voluntary tab */
import UIKit

class Voluntary: UIViewController {
    
    
    @IBOutlet weak var alertMsg: UILabel!
    
    @IBOutlet weak var endPeaker: UIDatePicker!
    @IBOutlet weak var startPeaker: UIDatePicker!
    
    @IBOutlet weak var okBtn: UIButton!
    var difference = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        alertMsg.isHidden = true
        setLimits()
        print("voluntary view did load")

    }
    @IBAction func next()
    {
        let event = Event(dateStart: startPeaker.date, dateEnd: endPeaker.date)
        
        if(difference > 5  )
        {
            event.isLong = true
        }

        self.performSegue(withIdentifier: "showValence", sender: event)

    }
    @IBAction func TimeChanged()
    {
        print("TimeChanged")
            var tmp = Calendar.current.dateComponents([.hour, .minute], from: startPeaker.date, to: endPeaker.date)
        
        difference = tmp.hour!*60 + tmp.minute!

        if(saver!.checkValidity(fromTime: startPeaker.date, untilTime:  endPeaker.date ))
        {
            // do not allow saving of event more than 5 min
            if( difference > 5  )
            {
                alertMsg.isHidden = false
                alertMsg.text = "Emotions are ususaly short. \n Please give a range \nat least < 5 minutes "
                alertMsg.textColor = UIColor.red
                okBtn.isHidden = true
            }
            else{
                alertMsg.isHidden = true
                okBtn.isHidden = false
                
            }
        }
        else{ // do not allow for saving an event at a time containing another event
            alertMsg.isHidden = false
            alertMsg.text = "You already have a saved emotion at those times. check the graph to see your saved emotions"
            alertMsg.textColor = UIColor.red

            okBtn.isHidden = true
            
        }
        
        setLimits()

    }
    // check that you cant save a begining date after the end date and an end date later than now
    func setLimits()
    {
        endPeaker.maximumDate = Date()
        startPeaker.maximumDate = endPeaker.date
        endPeaker.minimumDate = startPeaker.date
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let event = sender as? Event  else { return }

        if let destination = segue.destination as? Valence{
            destination.event = event
        }
        else  if let destination = segue.destination as? Rating{
        
            destination.event = event
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear think")
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        if(saver!.newData)
        {
            saver!.newData = false
            self.tabBarController?.selectedIndex = 2;
        }
    }
    
    
}
